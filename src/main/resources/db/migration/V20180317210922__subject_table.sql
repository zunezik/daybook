CREATE TABLE subjects
(
  id SERIAL PRIMARY KEY,
  name VARCHAR(40),
  day_of_week VARCHAR(40),
  lesson_number INT
);