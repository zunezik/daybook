CREATE TABLE users
(
  id SERIAL PRIMARY KEY,
  first_name VARCHAR(40),
  last_name VARCHAR(40),
  username VARCHAR(40),
  u_password VARCHAR(60),
  u_role VARCHAR(40)
);