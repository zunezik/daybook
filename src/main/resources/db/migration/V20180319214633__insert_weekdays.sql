CREATE TABLE weekdays
(
  id      SERIAL PRIMARY KEY,
  weekday VARCHAR(40),
  lesson1 VARCHAR(40),
  lesson2 VARCHAR(40),
  lesson3 VARCHAR(40),
  lesson4 VARCHAR(40)
);

INSERT INTO weekdays (id, weekday) VALUES
  (1, 'Monday'),
  (2, 'Tuesday'),
  (3, 'Wednesday'),
  (4, 'Thursday'),
  (5, 'Friday'),
  (6, 'Saturday'),
  (7, 'Sunday');