CREATE TABLE absences
(
  id         SERIAL PRIMARY KEY,
  subject_id INT       NOT NULL,
  student_id INT       NOT NULL,
  present    BOOLEAN   NOT NULL,
  date       TIMESTAMP NOT NULL
);