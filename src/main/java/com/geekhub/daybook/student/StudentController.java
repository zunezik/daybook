package com.geekhub.daybook.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/student")
public class StudentController {

    private final StudentValidator studentValidator;
    private final StudentRepository studentRepository;

    @Autowired
    public StudentController(StudentValidator studentValidator, StudentRepository studentRepository) {
        this.studentValidator = studentValidator;
        this.studentRepository = studentRepository;
    }

    @GetMapping
    public String showAllStudents(Model model) {
        model.addAttribute("students", studentRepository.getAllStudents());

        return "students";
    }

    @GetMapping("/createStudent")
    public String showCreationPage(Model model) {
        model.addAttribute("student", new Student());

        return "/createStudent";
    }

    @PostMapping("/createStudent")
    public String processStudentCreation(@ModelAttribute("student") Student student, BindingResult bindingResult) {
        studentValidator.validate(student, bindingResult);

        if (bindingResult.hasErrors()) {
            return "createStudent";
        }

        studentRepository.saveStudent(student);

        return "redirect:/student";
    }

    @PostMapping("/remove/{id}")
    public String deleteStudent(@PathVariable Long id) {
        studentRepository.deleteStudent(id);

        return "redirect:/student";
    }
}
