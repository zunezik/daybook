package com.geekhub.daybook.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class StudentRepository {

    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert studentInsert;

    @Autowired
    public StudentRepository(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.studentInsert = new SimpleJdbcInsert(dataSource)
                .withTableName("students")
                .usingGeneratedKeyColumns("id");
    }

    public Student saveStudent(Student student) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("first_name", student.getFirstName());
        parameters.put("last_name", student.getLastName());
        parameters.put("s_group", student.getGroup());
        Number newId = studentInsert.executeAndReturnKey(parameters);
        student.setId(newId.intValue());

        return student;
    }

    public List<Student> getAllStudents() {
        String sql = "SELECT id, first_name, last_name, s_group FROM students";

        return this.jdbcTemplate.query(sql, new StudentRowMapper());
    }

    public void deleteStudent(Long id) {
        String sql = "DELETE FROM students WHERE id = ?";
        this.jdbcTemplate.update(sql, id);
    }
}
