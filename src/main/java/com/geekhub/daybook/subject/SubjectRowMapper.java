package com.geekhub.daybook.subject;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SubjectRowMapper  implements RowMapper<Subject> {

    @Override
    public Subject mapRow(ResultSet rs, int rowNum) throws SQLException {
        Subject subject = new Subject();
        subject.setId(rs.getInt("id"));
        subject.setName(rs.getString("name"));
        subject.setDayOfWeek(rs.getString("day_of_week"));
        subject.setLessonNumber(rs.getInt("lesson_number"));

        return subject;
    }
}
