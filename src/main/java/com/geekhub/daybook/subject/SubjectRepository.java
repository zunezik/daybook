package com.geekhub.daybook.subject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class SubjectRepository {

    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert subjectInsert;

    @Autowired
    public SubjectRepository(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.subjectInsert = new SimpleJdbcInsert(dataSource)
                .withTableName("subjects")
                .usingGeneratedKeyColumns("id");
    }

    public Subject saveSubject(Subject subject) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("name", subject.getName());
        parameters.put("day_of_week", subject.getDayOfWeek());
        parameters.put("lesson_number", subject.getLessonNumber());
        Number newId = subjectInsert.executeAndReturnKey(parameters);
        subject.setId(newId.intValue());

        return subject;
    }

    public List<Subject> getAllSubjects() {
        String sql = "SELECT id, name, day_of_week, lesson_number FROM subjects";

        return this.jdbcTemplate.query(sql, new SubjectRowMapper());
    }

    public List<Subject> getSubjectsByName(List<String> names) {
        String separatedNames = names.stream()
                .map(i -> "'" + i + "'")
                .collect(Collectors.joining(","));

        String sql = "SELECT id, name, day_of_week, lesson_number FROM subjects WHERE name IN (" + separatedNames + ")";

        return this.jdbcTemplate.query(sql, new SubjectRowMapper());
    }

    public void deleteSubject(Long id) {
        String sql = "DELETE FROM subjects WHERE id = ?";
        this.jdbcTemplate.update(sql, id);
    }
}
