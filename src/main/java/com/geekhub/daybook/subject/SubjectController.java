package com.geekhub.daybook.subject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/subject")
public class SubjectController {

    private final SubjectValidator subjectValidator;
    private final SubjectRepository subjectRepository;

    @Autowired
    public SubjectController(SubjectValidator subjectValidator, SubjectRepository subjectRepository) {
        this.subjectValidator = subjectValidator;
        this.subjectRepository = subjectRepository;
    }

    @GetMapping
    public String showAllSubjects(Model model) {
        model.addAttribute("subjects", subjectRepository.getAllSubjects());

        return "subjects";
    }

    @GetMapping("/createSubject")
    public String showCreationPage(Model model) {
        model.addAttribute("subject", new Subject());

        return "/createSubject";
    }

    @PostMapping("/createSubject")
    public String processStudentCreation(@ModelAttribute("subject") Subject subject, BindingResult bindingResult) {
        subjectValidator.validate(subject, bindingResult);

        if (bindingResult.hasErrors()) {
            return "createSubject";
        }

        subjectRepository.saveSubject(subject);

        return "redirect:/subject";
    }

    @PostMapping("/remove/{id}")
    public String deleteSubject(@PathVariable Long id) {
        subjectRepository.deleteSubject(id);

        return "redirect:/subject";
    }
}
