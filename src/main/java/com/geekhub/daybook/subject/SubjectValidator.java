package com.geekhub.daybook.subject;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class SubjectValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Subject.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Subject subject = (Subject) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dayOfWeek", "Required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lessonNumber", "Required");

        if (subject.getLessonNumber() > 4 || subject.getLessonNumber() <= 0) {
            errors.rejectValue("lessonNumber", "Size.subjectForm.lessonNumber");
        }
    }
}
