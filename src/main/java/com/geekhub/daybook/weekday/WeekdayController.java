package com.geekhub.daybook.weekday;

import com.geekhub.daybook.subject.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/weekday")
public class WeekdayController {

    private final WeekdayRepository weekdayRepository;
    private final SubjectRepository subjectRepository;
    private String selectedDay;

    @Autowired
    public WeekdayController(WeekdayRepository weekdayRepository, SubjectRepository subjectRepository) {
        this.weekdayRepository = weekdayRepository;
        this.subjectRepository = subjectRepository;
    }

    @GetMapping
    public String selectWeekday() {
        return "weekdaySelect";
    }

    @PostMapping
    public String loadSetupPage(@RequestParam String weekday) {
        selectedDay = weekday;

        return "redirect:/weekday/setup";
    }

    @PostMapping("/setup")
    public String setupWeekday(@ModelAttribute("weekday") Weekday weekday) {
        weekdayRepository.updateWeekday(weekday.getId(), weekday.getLesson1(),
                weekday.getLesson2(),
                weekday.getLesson3(),
                weekday.getLesson4());

        return "weekdaySelect";
    }

    @GetMapping("/setup")
    public String showSetupPage(Model model) {
        model.addAttribute("selectedDay", selectedDay);
        model.addAttribute("subjects", subjectRepository.getAllSubjects());
        model.addAttribute("weekday", weekdayRepository.getWeekdayByName(selectedDay));

        return "weekdaySetup";
    }
}
