package com.geekhub.daybook.weekday;

public class Weekday {

    private int id;
    private String weekday;
    private String lesson1;
    private String lesson2;
    private String lesson3;
    private String lesson4;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWeekday() {
        return weekday;
    }

    public void setWeekday(String weekday) {
        this.weekday = weekday;
    }

    public String getLesson1() {
        return lesson1;
    }

    public void setLesson1(String lesson1) {
        this.lesson1 = lesson1;
    }

    public String getLesson2() {
        return lesson2;
    }

    public void setLesson2(String lesson2) {
        this.lesson2 = lesson2;
    }

    public String getLesson3() {
        return lesson3;
    }

    public void setLesson3(String lesson3) {
        this.lesson3 = lesson3;
    }

    public String getLesson4() {
        return lesson4;
    }

    public void setLesson4(String lesson4) {
        this.lesson4 = lesson4;
    }
}
