package com.geekhub.daybook.weekday;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class WeekdayRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public WeekdayRepository(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void updateWeekday(int id, String lesson1, String lesson2, String lesson3, String lesson4) {
        String sql = "UPDATE weekdays SET lesson1 = ?, lesson2 = ?, lesson3 = ?, lesson4 = ? WHERE id = ?";
        this.jdbcTemplate.update(sql, lesson1, lesson2, lesson3, lesson4, id);
    }

    public Weekday getWeekdayByName(String weekday) {
        String sql = "SELECT id, weekday, lesson1, lesson2, lesson3, lesson4 FROM weekdays WHERE weekday = ?";

        return (Weekday) this.jdbcTemplate.queryForObject(sql, new Object[]{weekday}, new WeekdayRowMapper());
    }

    public List<String> getLessonsByWeekdayId(int id) {
        String sql = "SELECT id, weekday, lesson1, lesson2, lesson3, lesson4 FROM weekdays WHERE id = ?";
        Weekday weekday = (Weekday) this.jdbcTemplate.queryForObject(sql, new Object[]{id}, new WeekdayRowMapper());

        List<String> lessons = new ArrayList<>();
        if (!weekday.getLesson1().equals("")) {
            lessons.add(weekday.getLesson1());
        }
        if (!weekday.getLesson2().equals("")) {
            lessons.add(weekday.getLesson2());
        }
        if (!weekday.getLesson3().equals("")) {
            lessons.add(weekday.getLesson3());
        }
        if (!weekday.getLesson4().equals("")) {
            lessons.add(weekday.getLesson4());
        }

        return lessons;
    }
}
