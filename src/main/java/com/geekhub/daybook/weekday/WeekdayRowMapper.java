package com.geekhub.daybook.weekday;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WeekdayRowMapper implements RowMapper {
    
    @Override
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        Weekday weekday = new Weekday();
        weekday.setId(rs.getInt("id"));
        weekday.setWeekday(rs.getString("weekday"));
        weekday.setLesson1(rs.getString("lesson1"));
        weekday.setLesson2(rs.getString("lesson2"));
        weekday.setLesson3(rs.getString("lesson3"));
        weekday.setLesson4(rs.getString("lesson4"));

        return weekday;
    }
}
