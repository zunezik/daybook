package com.geekhub.daybook.homePage;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class HomePageController {

    @GetMapping
    public String getMainPage() {
        return "index";
    }
}
