package com.geekhub.daybook.registration;

import com.geekhub.daybook.user.UserRepository;
import com.geekhub.daybook.user.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/registration")
public class RegistrationController {

    private final UserRepository userRepository;
    private final RegistrationService registrationService;
    private final UserValidator userValidator;

    @Autowired
    public RegistrationController(UserRepository userRepository, RegistrationService registrationService, UserValidator userValidator) {
        this.userRepository = userRepository;
        this.registrationService = registrationService;
        this.userValidator = userValidator;
    }

    @GetMapping
    public String showRegistrationPage(Model model) {
        model.addAttribute("registrationForm", new Registration());

        return "registration";
    }

    @PostMapping
    public String processRegistration(@ModelAttribute("registrationForm") Registration registration, BindingResult bindingResult) {
        userValidator.validate(registration, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        this.registrationService.register(registration);

        return "redirect:login";
    }
}
