package com.geekhub.daybook.absence;

import com.geekhub.daybook.student.StudentRepository;
import com.geekhub.daybook.subject.Subject;
import com.geekhub.daybook.subject.SubjectRepository;
import com.geekhub.daybook.weekday.WeekdayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/absence")
public class AbsenceController {

    private final WeekdayRepository weekdayRepository;
    private final StudentRepository studentRepository;
    private final SubjectRepository subjectRepository;
    private final AbsenceRepository absenceRepository;

    @Autowired
    public AbsenceController(WeekdayRepository weekdayRepository, StudentRepository studentRepository, SubjectRepository subjectRepository, AbsenceRepository absenceRepository) {
        this.weekdayRepository = weekdayRepository;
        this.studentRepository = studentRepository;
        this.subjectRepository = subjectRepository;
        this.absenceRepository = absenceRepository;
    }

    @PostMapping
    public String showPage(Model model, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate inputDate) {
        List<String> lessonsByWeekdayId = weekdayRepository.getLessonsByWeekdayId(inputDate.getDayOfWeek().getValue());
        List<Subject> subjects = subjectRepository.getSubjectsByName(lessonsByWeekdayId);
        model.addAttribute("subjects", subjects);
        model.addAttribute("date", inputDate);

        return "subjectSelection";
    }

    @PostMapping("/submit")
    public String submitAbsences(@ModelAttribute Absence absence) {
        absenceRepository.saveAbsence(absence);

        return "index";
    }

    @GetMapping("select/{subjectId}/{date}/{subjectName}")
    public String selectAbsentStudents(Absence absence, Model model) {
        model.addAttribute("absence", absence);
        model.addAttribute("students", studentRepository.getAllStudents());

        return "absence";
    }
}
