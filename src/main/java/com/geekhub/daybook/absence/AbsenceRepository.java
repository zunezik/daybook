package com.geekhub.daybook.absence;

import com.geekhub.daybook.student.Student;
import com.geekhub.daybook.student.StudentRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

@Repository
public class AbsenceRepository {

    private final JdbcTemplate jdbcTemplate;
    private final AbsenceService absenceService;

    @Autowired
    public AbsenceRepository(DataSource dataSource, AbsenceService absenceService) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.absenceService = absenceService;
    }

    public List<Student> getAbsentStudentsByDate(LocalDate date) {
        String sql = "SELECT id, first_name, last_name, s_group FROM students s INNER JOIN absences a ON s.id = a.student_id;";

        return this.jdbcTemplate.query(sql, new StudentRowMapper());
    }

    public void saveAbsence(Absence absence) {
        String sql = "INSERT INTO absences (subject_id, student_id, present, date) VALUES (?, ?, ?, ?)";

        List<PreparedForInsertAbsence> preparedAbsences = absenceService.prepareInsert(absence);

        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                PreparedForInsertAbsence preparedAbsence = preparedAbsences.get(i);
                ps.setInt(1, preparedAbsence.getSubjectId());
                ps.setInt(2, preparedAbsence.getStudentId());
                ps.setBoolean(3, preparedAbsence.isPresent());
                ps.setObject(4, preparedAbsence.getDate());
            }

            @Override
            public int getBatchSize() {
                return preparedAbsences.size();
            }
        });
    }
}
