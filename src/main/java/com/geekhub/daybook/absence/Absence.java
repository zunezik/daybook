package com.geekhub.daybook.absence;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

public class Absence {

    private int subjectId;
    private String subjectName;
    private List<Integer> presentStudentsIds;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public List<Integer> getPresentStudentsIds() {
        return presentStudentsIds;
    }

    public void setPresentStudentsIds(List<Integer> presentStudentsIds) {
        this.presentStudentsIds = presentStudentsIds;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }
}
