package com.geekhub.daybook.absence;

import com.geekhub.daybook.student.Student;
import com.geekhub.daybook.student.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AbsenceService {

    private final StudentRepository studentRepository;

    @Autowired
    public AbsenceService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<PreparedForInsertAbsence> prepareInsert(Absence absence) {
        List<Integer> presentStudentsIds = absence.getPresentStudentsIds();
        List<PreparedForInsertAbsence> preparedAbsences = new ArrayList<>();
        List<Student> allStudents = studentRepository.getAllStudents();

        for (Student student : allStudents) {
            PreparedForInsertAbsence preparedAbsence = new PreparedForInsertAbsence();

            if (presentStudentsIds.contains(student.getId())) {
                preparedAbsence.setPresent(true);
            } else {
                preparedAbsence.setPresent(false);
            }

            preparedAbsence.setStudentId(student.getId());
            preparedAbsence.setSubjectId(absence.getSubjectId());
            preparedAbsence.setDate(absence.getDate());

            preparedAbsences.add(preparedAbsence);
        }

        return preparedAbsences;
    }
}
