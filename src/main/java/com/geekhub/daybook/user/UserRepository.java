package com.geekhub.daybook.user;

import com.geekhub.daybook.role.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.*;

@Repository
public class UserRepository {

    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert userInsert;

    @Autowired
    public UserRepository(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.userInsert = new SimpleJdbcInsert(dataSource)
                .withTableName("users")
                .usingGeneratedKeyColumns("id");
    }

    public User saveUser(User user) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("first_name", user.getFirstName());
        parameters.put("last_name", user.getLastName());
        parameters.put("username", user.getUsername());
        parameters.put("u_password", user.getPassword());
        parameters.put("u_role", Role.USER);
        Number newId = userInsert.executeAndReturnKey(parameters);
        user.setId(newId.intValue());

        return user;
    }

    public Optional<User> findUserByUsername(String username) {
        String sql = "SELECT id, first_name, last_name, username, u_password, u_role FROM users WHERE username = ?";
        User user;
        try {
            user = this.jdbcTemplate.queryForObject(sql, new Object[]{username}, new UsersRowMapper());
            return Optional.of(user);
        } catch (DataAccessException e) {
            return Optional.empty();
        }
    }
}
